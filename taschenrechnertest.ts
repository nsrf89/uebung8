$(function (){

    function addition(x:number, y:number) : number {
        return x+y;
    }

    function subtraktion(x:number, y:number) : number {
        return x-y;
    }

    function multiplikation(x:number, y:number) : number {
        return x*y;
    }

    function division(x:number, y:number) : number {
        return x/y;
    }

    function flaecherecteck(x:number, y:number) : number {
        return multiplikation(x,y);
    }

    function umfangrechteck(x:number, y:number) : number{
        return 2*(addition(x,y));
    }

    function potenz(x:number, y:number) : number {
        return x**y;
    }

    function flaechekreis(x:number) : number {
        return Math.PI*x*x;
    }

    function umfangkreis(x:number) : number {
        return 2*Math.PI*x;
    }

    function faculty(x: number): number {
        if (x == 0) {
            return 1;
        } else {
            return x * faculty(x - 1);
        }
    }

    function eulersche(x: number): number {
        let e: number = 0;
        while (x >= 0) {
            e = e + (1 / faculty(x));
            x--;
        }
        return e;
    }

    function modulo(x:number, y:number) : number {
        return x%y;
    }

    function dual (x:number) : string{
        let b : number[] = [];
        let t : number;
        let reversed : number[];
        let str : string;
        while (x > 0) {
            b.push(x%2);
            t = Math.floor(x/2);
            x = t;
        }
        reversed = b.reverse();
        str =  reversed.join("");
        return str;
    }



    $("#plus").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = addition(x,y);
        $("#ergebnis").val(result);
    });

    $("#minus").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = subtraktion(x,y);
        $("#ergebnis").val(result);
    });

    $("#mal").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = multiplikation(x,y);
        $("#ergebnis").val(result);
    });

    $("#geteilt").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = division(x,y);
        $("#ergebnis").val(result);
    });

    $("#FR").on("click", ()=> {
        $("#number1").val("");
        $("#number2").val("");
        $("#ergebnis").val("");
    });

    $("#FR").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = flaecherecteck(x,y);
        $("#ergebnis").val(result);
    });

    $("#UR").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = umfangrechteck(x,y);
        $("#ergebnis").val(result);
    });

    $("#potenz").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = potenz(x,y);
        $("#ergebnis").val(result);
    });

    $("#FK").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let x  : number = Number(DOMx.val());
        let result  : number = flaechekreis(x);
        $("#ergebnis").val(result);
    });

    $("#UK").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let x  : number = Number(DOMx.val());
        let result  : number = umfangkreis(x);
        $("#ergebnis").val(result);
    });

    $("#fct").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let x  : number = Number(DOMx.val());
        let result  : number = faculty(x);
        $("#ergebnis").val(result);
    });

    $("#eul").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let x  : number = Number(DOMx.val());
        let result  : number = eulersche(x);
        $("#ergebnis").val(result);
    });

    $("#modulo").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let DOMy : JQuery = $("#number2");
        let x  : number = Number(DOMx.val());
        let y : number = Number(DOMy.val());
        let result  : number = modulo(x,y);
        $("#ergebnis").val(result);
    });

    $("#dual").on( 'click', () => {
        let DOMx : JQuery = $("#number1");
        let x  : number = Number(DOMx.val());
        let result  : string = dual(x);
        $("#ergebnis").val(result);
    });

});